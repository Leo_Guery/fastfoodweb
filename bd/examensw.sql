

--
-- Base de datos: `examensw`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
`categoria_id` bigint(20) NOT NULL,
  `categoria_nombre` varchar(255) DEFAULT NULL,
  `categoria_descripcion` varchar(255) DEFAULT NULL,
  `categoria_padre` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`categoria_id`, `categoria_nombre`, `categoria_descripcion`, `categoria_padre`) VALUES
(1, 'Sandwiches', '', 0),
(2, 'Bebidas', '', 0),
(3, 'Entradas', '', 0),
(4, 'Ensaladas', '', 0),
(5, 'Combos', '', 0),
(6, 'Pastas', '', 0),
(7, 'Parrilla', '', 0),
(8, 'Milanesas', '', 0),
(9, 'Guarniciones', '', 0),
(10, 'Postres', '', 25),
(11, 'Adicionales', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
`cliente_id` bigint(20) NOT NULL,
  `cliente_nombre` varchar(255) DEFAULT NULL,
  `cliente_ape_paterno` varchar(255) DEFAULT NULL,
  `cliente_ape_materno` varchar(255) DEFAULT NULL,
  `cliente_email` varchar(255) DEFAULT NULL,
  `cliente_telefono` varchar(255) DEFAULT NULL,
  `cliente_telefono_movil` varchar(255) DEFAULT NULL,
  `cliente_password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25011 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`cliente_id`, `cliente_nombre`, `cliente_ape_paterno`, `cliente_ape_materno`, `cliente_email`, `cliente_telefono`, `cliente_telefono_movil`, `cliente_password`) VALUES
(1, 'Oscar', 'Cárdenas', 'Albornoz', 'cardenas1108@gmail.com', '9338112', '83055080', 'martincr8263'),
(2, 'Oscar', 'Cárdenas', 'Albornoz', 'mudvayne450@hotmail.com', '9338112', '9338112', 'martincr8263'),
(3, 'Oscar', 'Cárdenas', 'Albornoz', 'cardenas1108@hotmail.com', '9338112', '9338112', '123'),
(4, 'gaspar', 'ccc', 'ccc', 'ggzhack@gmail.com', '7089604', '70896046', '123'),
(5, 'edd', 'cc', 'ccc', 'edson_cht@hotmail.com', '789898', '68804181', '123'),
(6, 'Juan', 'Junior', 'Barboza', 'cat_eton@hotmail.com', '4364577', '70947376', '123456'),
(7, 'juan', 'martinez', 'cabrera', 'martinez@hotmail.com', '5657678', '66756757', '123456'),
(8, 'guery', 'chumacero', 'romano', 'guerychumacero@gmail.com', '334354a', '77600892', 'PlasmaRelampago');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comuna`
--

CREATE TABLE IF NOT EXISTS `comuna` (
`comuna_id` bigint(20) NOT NULL,
  `comuna_nombre` varchar(255) DEFAULT NULL,
  `comuna_descripcion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comuna`
--

INSERT INTO `comuna` (`comuna_id`, `comuna_nombre`, `comuna_descripcion`) VALUES
(1, 'Plan 3000', NULL),
(2, 'Villa 1ro de Mayo', NULL),
(3, 'La Pampa', NULL),
(4, 'Los Lotes', NULL),
(5, 'La Cuchilla', NULL),
(6, 'Los Pozos', NULL),
(7, 'San Aurelio', NULL),
(8, 'Avenida Pirai', NULL),
(9, 'La Florida', NULL),
(10, 'La Granja', NULL),
(27, 'Puente Alto', NULL),
(30, 'Recoleta', NULL),
(33, 'San Joaquín', NULL),
(34, 'San José de Maipo', NULL),
(36, 'San Ramón', NULL),
(37, 'Santiago', NULL),
(38, 'Vitacura', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE IF NOT EXISTS `configuracion` (
`configuracion_id` bigint(20) NOT NULL,
  `configuracion_nombre` varchar(255) DEFAULT NULL,
  `configuracion_descripcion` varchar(255) DEFAULT NULL,
  `configuracion_valor` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`configuracion_id`, `configuracion_nombre`, `configuracion_descripcion`, `configuracion_valor`) VALUES
(1, 'Imágenes Productos', 'Ruta para la subida de imágenes de los productos', 'C:\\xampp\\htdocs\\carro_compras\\images\\productos'),
(2, 'Imágenes Slider', 'Ruta para subida de imágenes del slider', 'C:\\xampp\\htdocs\\carro_compras\\images\\nivo-slider'),
(3, 'E-Mail del administrador del sistema', 'E-Mail del administrador del sistema', 'edson.cht69@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion`
--

CREATE TABLE IF NOT EXISTS `direccion` (
`direccion_id` bigint(20) NOT NULL,
  `direccion_descripcion` varchar(255) DEFAULT NULL,
  `comuna_id` bigint(20) DEFAULT NULL,
  `cliente_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `direccion`
--

INSERT INTO `direccion` (`direccion_id`, `direccion_descripcion`, `comuna_id`, `cliente_id`) VALUES
(4, 'Las Tranqueras 450, depto. 13', 9, 1),
(5, 'barrio bibosi', 3, 25010);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forma_pago`
--

CREATE TABLE IF NOT EXISTS `forma_pago` (
`forma_pago_id` bigint(20) NOT NULL,
  `forma_pago_nombre` varchar(255) DEFAULT NULL,
  `forma_pago_descripcion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `forma_pago`
--

INSERT INTO `forma_pago` (`forma_pago_id`, `forma_pago_nombre`, `forma_pago_descripcion`) VALUES
(1, 'Efectivo', 'Se paga al momento de entregar el pedido');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen`
--

CREATE TABLE IF NOT EXISTS `imagen` (
`imagen_id` bigint(20) NOT NULL,
  `imagen_nombre` varchar(255) DEFAULT NULL,
  `producto_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `imagen`
--

INSERT INTO `imagen` (`imagen_id`, `imagen_nombre`, `producto_id`) VALUES
(37, 'PI-000037-PI-000036.jpg', 7),
(38, 'PI-000038-agua_mineral.jpg', 8),
(41, 'PI-000041-Coca_Cola_500.jpg', 9),
(42, 'PI-000042-Vino_Fino.jpg', 10),
(43, 'PI-000043-Vino_R._Famoso.jpg', 6),
(44, 'PI-000044-Vino_Lopez.jpg', 11),
(45, 'PI-000045-Cerveza_Quilmes354cc.jpg', 12),
(46, 'PI-000046-Gaseosa_Speed_500.jpg', 13),
(48, 'PI-000048-Arrollado_de_Pollo.jpg', 14),
(50, 'PI-000050-Arrollado_de_Pollo_p2.jpg', 15),
(51, 'PI-000051-Combo_Spiedo.jpg', 16),
(94, 'PI-000094-Milanesa_Ternera.jpg', 48),
(95, 'PI-000095-Milanesa_Ternera_Napo.jpg', 49),
(96, 'PI-000096-Milanesa_Ternera_Fugazeta.jpg', 50),
(97, 'PI-000097-Milanesa_de_Pollo.jpg', 51),
(98, 'PI-000098-Milanesa_de_Pollo_Napolitana.jpg', 52),
(100, 'PI-000100-Milanesa_de_Pollo_Fugazzeta.jpg', 53),
(101, 'PI-000101-Milanesa_de_Pollo_Suiza.jpg', 54),
(102, 'PI-000102-Milanesa_de_Soja.jpg', 55),
(107, 'PI-000107-Combo_Ravioles.jpg', 56),
(108, 'PI-000108-Combo_Noquis.jpg', 57),
(109, 'PI-000109-Combo_Noquis_Papa.jpg', 58),
(111, 'PI-000111-Fideos_Salteados_cVerd_pComp.jpg', 59),
(113, 'PI-000113-Fideos_Salteados_cVerd.jpg', 61),
(114, 'PI-000114-Combo_Ravioles_caseros.jpg', 62),
(115, 'PI-000115-Combo_Ravioles_caseros_pComp.jpg', 63),
(116, 'PI-000116-Combo_Tallarines_al_Huevo.jpg', 64),
(117, 'PI-000117-Combo_Tallarines_al_Huevo_pComp.jpg', 65),
(118, 'PI-000118-Combo_Lasagna_Iraliana.jpg', 66),
(119, 'PI-000119-Combo_Lasagna_Iraliana_pComp.jpg', 67),
(120, 'PI-000120-Combo_Canelones.jpg', 68),
(121, 'PI-000121-Combo_Canelones_pComp.jpg', 69),
(122, 'PI-000122-Combo_Estofado.jpg', 70),
(123, 'PI-000123-Combo_Macarrones_Graten_de_verduras.jpg', 71),
(124, 'PI-000124-Parrilla_Matambre_a_la_Pizza.jpg', 72),
(125, 'PI-000125-Parrilla_Bife_de_Chorizo_md.jpg', 73),
(126, 'PI-000126-Parrilla_Suprema_Parrilla.jpg', 74),
(127, 'PI-000127-Parrilla_Chorizo.jpg', 75),
(128, 'PI-000128-Parrilla_Morcilla.jpg', 76),
(129, 'PI-000129-Parrilla_Pollo_a_la_Parrilla.jpg', 77),
(130, 'PI-000130-Parrilla_Pollo_a_la_Parrilla_con_Guarn.jpg', 78),
(131, 'PI-000131-Parrilla_Brochette_Mixta.jpg', 79),
(132, 'PI-000132-Parrilla_Chimichurri_gde.jpg', 80),
(133, 'PI-000133-Parrilla_Suprema_Parrilla.jpg', 81),
(134, 'PI-000134-Parrilla_Supremita_cGuarnicion.jpg', 82),
(135, 'PI-000135-Parrilla_Combo_Pollo_Parrillero_4p.jpg', 83),
(136, 'PI-000136-Parrilla_Choripan.jpg', 84),
(137, 'PI-000137-Guarniciones_Tortilla_de_Papa.jpg', 85),
(138, 'PI-000138-Guarniciones_Tortilla_de_Verduras.jpg', 86),
(139, 'PI-000139-Guarniciones_Papas_Dado.jpg', 87),
(141, 'PI-000141-Guarniciones_Papas_Fritas.jpg', 88),
(142, 'PI-000142-Guarniciones_Arroz_Primavera.jpg', 89),
(143, 'PI-000143-Guarniciones_Verduras_Salteadas.jpg', 90),
(144, 'PI-000144-Guarniciones_Acelga_a_la_Crema.jpg', 91),
(145, 'PI-000145-Guarniciones_Pure_de_Papas.jpg', 92),
(146, 'PI-000146-Guarniciones_Pure_de_Calabaza.jpg', 93),
(147, 'PI-000147-Guarniciones_Pure_Mixto.jpg', 94),
(148, 'PI-000148-Guarniciones_Revuelto_Gramajo.jpg', 95),
(149, 'PI-000149-Guarniciones_Papas_a_la_Crema.jpg', 96),
(150, 'PI-000150-Guarniciones_Chop_suey.jpg', 97),
(151, 'PI-000151-Guarniciones_Zanahorias_a_la_Crema.jpg', 98),
(152, 'PI-000152-Guarniciones_Bocadillos.jpg', 99),
(153, 'PI-000153-Guarniciones_Papas_y_Batatas_al_horno.jpg', 100),
(160, 'PI-000160-Guarniciones_Tortilla_Española.jpg', 101),
(161, 'PI-000161-Ensalada_3_Ingredientes.jpg', 102),
(162, 'PI-000162-Ensalada_Especial_salp_angus.jpg', 103),
(163, 'PI-000163-Ensalada_para_compartir.jpg', 104),
(164, 'PI-000164-Ensalada_Dietetica.jpg', 105),
(165, 'PI-000165-Ensalada_Recargo_Pollo.jpg', 106),
(166, 'PI-000166-Postres_Flan_Casero.jpg', 107),
(167, 'PI-000167-Postres_Tiramisu.jpg', 108),
(168, 'PI-000168-Postres_Vainilla.jpg', 109),
(170, 'PI-000170-Postres_Tortas_Varias.jpg', 110),
(171, 'PI-000171-Postres_Tarta_de_Manzana.jpg', 111),
(172, 'PI-000172-Postres_Crema_Porcion.jpg', 112),
(173, 'PI-000173-Postres_Dulce_de_Leche.jpg', 113),
(174, 'PI-000174-Postres_Natillas.jpg', 114),
(175, 'PI-000175-Postres_Gelatina_con_Fruta.jpg', 115),
(176, 'PI-000176-Postres_Manzana_asada.jpg', 116),
(177, 'PI-000177-Postres_Budin_de_Pan_para_comp.jpg', 117),
(179, 'PI-000179-Postres_Tarantelas_para_comp.jpg', 118),
(180, 'PI-000180-Postres_Ensalada_de_Fruta_pComp.jpg', 119),
(181, 'PI-000181-Postres_Queso_y_Dulce.jpg', 120),
(182, 'PI-000182-Postres_Pastelitos.jpg', 121),
(183, 'PI-000183-Postres_Arroz_con_Leche.jpg', 122),
(184, 'PI-000184-Adicionales_Pan.jpg', 123),
(185, 'PI-000185-Adicionales_Aderezos.jpg', 124),
(186, 'PI-000186-Adicionales_Queso_Rallado.jpg', 125),
(187, 'PI-000187-Adicionales_Platos.jpg', 126),
(188, 'PI-000188-Adicionales_Cubiertos.jpg', 127),
(189, 'PI-000189-Adicionales_Vasos.jpg', 128),
(190, 'PI-000190-Adicionales_Huevo.jpg', 129),
(192, 'PI-000192-Sandwiches_Hamburquesa_cQueso.jpg', 130),
(193, 'PI-000193-Sandwiches_Hamburquesa_sola.jpg', 131),
(194, 'PI-000194-Sandwiches_Hamburquesa_Jamon_y_Queso.jpg', 132),
(195, 'PI-000195-Sandwiches_Hamburquesa_Jamon_y_Queso_T.jpg', 133),
(196, 'PI-000196-Sandwiches_Hamburquesa_saint_croissant.jpg', 134),
(197, 'PI-000197-Sandwiches_Sand_Saborizado_J_Q.jpg', 135),
(198, 'PI-000198-Sandwiches_Panini.jpg', 136),
(199, 'PI-000199-Sandwiches_Sand_Pavita_L_T.jpg', 137),
(200, 'PI-000200-Sandwiches_Milanesa_L_T.jpg', 138),
(201, 'PI-000201-200px-Escudo_FICCT.png', 139),
(202, 'PI-000202-bolivia.png', 140);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_panel_control_cliente`
--

CREATE TABLE IF NOT EXISTS `menu_panel_control_cliente` (
`menu_panel_control_cliente_id` bigint(20) NOT NULL,
  `menu_panel_control_cliente_nombre` varchar(255) DEFAULT NULL,
  `menu_panel_control_cliente_url` varchar(255) DEFAULT NULL,
  `menu_panel_control_cliente_orden` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu_panel_control_cliente`
--

INSERT INTO `menu_panel_control_cliente` (`menu_panel_control_cliente_id`, `menu_panel_control_cliente_nombre`, `menu_panel_control_cliente_url`, `menu_panel_control_cliente_orden`) VALUES
(1, 'Modificar mis Datos', 'site/modificarDatosCliente', 2),
(2, 'Ver Historial de mis Pedidos', 'site/historialDeMisPedidos', 3),
(3, 'Mis Direcciones de Envío', 'site/misDireccionesDeEnvio', 4),
(4, 'Inicio', 'site/panelControlCliente', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagina`
--

CREATE TABLE IF NOT EXISTS `pagina` (
`pagina_id` bigint(20) NOT NULL,
  `pagina_nombre` varchar(255) DEFAULT NULL,
  `pagina_titulo` varchar(255) DEFAULT NULL,
  `pagina_texto` text
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pagina`
--

INSERT INTO `pagina` (`pagina_id`, `pagina_nombre`, `pagina_titulo`, `pagina_texto`) VALUES
(1, 'Quienes Somos', 'Quienes Somos', '<p>P&aacute;gina de <strong><em>prueba</em></strong> creada con <strong>CKEditor</strong>.</p>\r\n'),
(2, 'Sobre Nosotros', '', '<table border="1" cellpadding="1" cellspacing="1" style="width: 100%;">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p style="margin: 0px; padding: 0px; color: rgb(146, 146, 146); font-family: Arial, Helvetica, sans-serif; font-size: 11px; background-color: rgb(249, 249, 249); line-height: 1.2em;"><small style="margin: 0px; padding: 0px; font-size: 1em;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.</small></p>\r\n\r\n			<p style="margin: 0px; padding: 0px; background-color: rgb(249, 249, 249); color: rgb(136, 136, 136); font-size: 1.2em; line-height: 1.4em; font-family: georgia, serif;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta.</p>\r\n			</td>\r\n			<td>\r\n			<p style="margin: 0px; padding: 0px; color: rgb(146, 146, 146); font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: normal; background-color: rgb(249, 249, 249);"><strong style="margin: 0px; padding: 0px; color: rgb(222, 3, 111);">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.</strong></p>\r\n\r\n			<p style="margin: 0px; padding: 0px; color: rgb(146, 146, 146); font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: normal; background-color: rgb(249, 249, 249);">Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</p>\r\n\r\n			<p style="margin: 0px; padding: 0px; color: rgb(146, 146, 146); font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: normal; background-color: rgb(249, 249, 249);">Maecenas ullamcorper, odio vel tempus egestas, dui orci faucibus orci, sit amet aliquet lectus dolor et quam. Pellentesque consequat luctus purus. Nunc et risus. Etiam a nibh. Phasellus dignissim metus eget nisi. Vestibulum sapien dolor, aliquet nec, porta ac, malesuada a, libero. Praesent feugiat purus eget est. Nulla facilisi. Vestibulum tincidunt sapien eu velit. Mauris purus. Maecenas eget mauris eu orci accumsan feugiat. Pellentesque eget velit. Nunc tincidunt.</p>\r\n			</td>\r\n			<td>\r\n			<p style="margin: 0px; padding: 0px; color: rgb(146, 146, 146); font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: normal; background-color: rgb(249, 249, 249);">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper</p>\r\n\r\n			<p style="margin: 0px; padding: 0px; color: rgb(146, 146, 146); font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: normal; background-color: rgb(249, 249, 249);"><strong style="margin: 0px; padding: 0px; color: rgb(222, 3, 111);">Maecenas ullamcorper, odio vel tempus egestas, dui orci faucibus orci, sit amet aliquet lectus dolor et quam. Pellentesque consequat luctus purus.</strong></p>\r\n\r\n			<p style="margin: 0px; padding: 0px; color: rgb(146, 146, 146); font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: normal; background-color: rgb(249, 249, 249);">Nunc et risus. Etiam a nibh. Phasellus dignissim metus eget nisi.</p>\r\n\r\n			<div class="divider" style="margin: 10px 0px; padding: 0px; clear: both; font-size: 0px; line-height: 0; height: 1px; background-color: rgb(221, 221, 221); overflow: hidden; color: rgb(146, 146, 146); font-family: Arial, Helvetica, sans-serif;">&nbsp;</div>\r\n\r\n			<p style="margin: 0px; padding: 0px; color: rgb(146, 146, 146); font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: normal; background-color: rgb(249, 249, 249);">To all of you, from all of us at Magento Demo Store - Thank you and Happy eCommerce!</p>\r\n\r\n			<p style="margin: 0px; padding: 0px; color: rgb(146, 146, 146); font-family: Arial, Helvetica, sans-serif; font-size: 11px; background-color: rgb(249, 249, 249); line-height: 1.2em;"><strong style="margin: 0px; padding: 0px; font-weight: normal; font-style: italic; font-size: 2em; line-height: normal; font-family: Georgia, serif;">John Doe</strong><br style="margin: 0px; padding: 0px;" />\r\n			<small style="margin: 0px; padding: 0px; font-size: 1em;">Some important guy</small></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE IF NOT EXISTS `pedido` (
`pedido_id` bigint(20) NOT NULL,
  `pedido_fecha` datetime NOT NULL,
  `pedido_cliente_nombre` varchar(255) NOT NULL,
  `pedido_cliente_direccion` varchar(255) NOT NULL,
  `pedido_cliente_comuna` varchar(255) NOT NULL,
  `pedido_cliente_provincia` varchar(255) NOT NULL,
  `pedido_cliente_telefono` varchar(255) NOT NULL,
  `pedido_cliente_telefono_movil` varchar(255) NOT NULL,
  `cliente_id` bigint(20) DEFAULT NULL,
  `forma_pago_id` bigint(20) DEFAULT NULL,
  `tipo_documento_id` bigint(20) DEFAULT NULL,
  `pedido_total` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`pedido_id`, `pedido_fecha`, `pedido_cliente_nombre`, `pedido_cliente_direccion`, `pedido_cliente_comuna`, `pedido_cliente_provincia`, `pedido_cliente_telefono`, `pedido_cliente_telefono_movil`, `cliente_id`, `forma_pago_id`, `tipo_documento_id`, `pedido_total`) VALUES
(7, '2013-01-28 21:48:07', 'Oscar', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '9338112', '83055080', 1, 1, 2, NULL),
(8, '2013-01-28 21:49:00', 'Oscar', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '9338112', '83055080', 1, 1, 2, NULL),
(10, '2013-02-19 15:09:36', 'Oscar', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '9338112', '83055080', 1, 1, 2, 21780),
(11, '2015-08-11 11:07:58', 'Oscar', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '9338112', '9338112', 3, 1, 2, 40160),
(12, '2015-08-11 15:34:31', 'Oscar', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '9338112', '9338112', 3, 1, 2, 20790),
(13, '2015-08-12 11:14:46', 'Oscar', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '9338112', '9338112', 3, 1, 2, 149190),
(16, '2015-11-17 09:35:57', 'edd', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '789898', '68804181', 5, 1, 2, 150),
(17, '2015-11-17 10:53:53', 'edd', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '789898', '68804181', 5, 1, 2, 203),
(18, '2015-11-17 10:55:41', 'edd', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '789898', '68804181', 5, 1, 2, 261),
(19, '2015-11-17 15:16:38', 'Juan', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '4364577', '70947376', 6, 1, 2, 145),
(20, '2015-11-17 15:31:41', 'Juan', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '4364577', '70947376', 6, 1, 2, 212),
(21, '2015-11-18 15:27:57', 'edd', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '789898', '68804181', 5, 1, 2, 230),
(22, '2015-11-18 15:28:27', 'edd', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '789898', '68804181', 5, 1, 2, 150),
(23, '2015-11-19 02:11:24', 'edd', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '789898', '68804181', 5, 1, 2, 130),
(24, '2016-03-22 10:26:29', 'Juan', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '4364577', '70947376', 6, 1, 2, 108),
(25, '2016-10-05 23:00:35', 'edd', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '789898', '68804181', 5, 1, 2, 116),
(26, '2017-04-20 17:47:48', 'Juan', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '4364577', '70947376', 6, 1, 2, 3000),
(27, '2017-04-24 17:44:16', 'Juan', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '4364577', '70947376', 6, 1, 2, 120),
(28, '2017-04-24 18:41:19', 'Juan', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '4364577', '70947376', 6, 1, 1, 230),
(29, '2017-04-24 21:30:28', 'guery', 'barrio bibosi', 'La Pampa', '', '334354a', '77600892', 25010, 1, 1, 560),
(30, '2017-04-26 20:31:48', 'Juan', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '4364577', '70947376', 6, 1, 1, 143),
(31, '2017-05-05 09:57:36', 'Juan', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '4364577', '70947376', 6, 1, NULL, 530),
(32, '2017-05-08 19:56:22', 'Juan', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '4364577', '76768268', 6, 1, NULL, 240),
(33, '2017-05-08 20:10:07', 'Juan', 'Las Tranqueras 450, depto. 13', 'La Florida', '', '4364577', '70947376', 6, 1, NULL, 115),
(34, '2017-05-08 20:51:56', 'Juan', 'barrio bibosi', 'La Pampa Barrio España C/Valencia nro 6', '', '4364577', '70947376', 6, 1, NULL, 385);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_detalle`
--

CREATE TABLE IF NOT EXISTS `pedido_detalle` (
`pedido_detalle_id` bigint(20) NOT NULL,
  `pedido_detalle_descripcion` varchar(255) DEFAULT NULL,
  `pedido_detalle_precio` int(11) DEFAULT NULL,
  `pedido_detalle_cantidad` int(11) DEFAULT NULL,
  `pedido_detalle_total` int(11) DEFAULT NULL,
  `pedido_id` bigint(20) DEFAULT NULL,
  `producto_id` bigint(20) DEFAULT NULL,
  `producto_codigo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido_detalle`
--

INSERT INTO `pedido_detalle` (`pedido_detalle_id`, `pedido_detalle_descripcion`, `pedido_detalle_precio`, `pedido_detalle_cantidad`, `pedido_detalle_total`, `pedido_id`, `producto_id`, `producto_codigo`) VALUES
(1, 'Hamburquesa c/J Q y Lechuga', 40, 1, 40, 16, 133, 'PR-00133'),
(2, 'Ensalada 3 Ingredientes', 45, 1, 45, 16, 102, 'PR-00102'),
(3, 'Milanesa de Pollo Suiza', 65, 1, 65, 16, 54, 'PR-00054'),
(4, 'Agua Mineral', 25, 1, 25, 17, 8, 'PR-00008'),
(5, 'Bife de Chorizo 1/2', 68, 1, 68, 17, 73, 'PR-00073'),
(6, 'Milanesa de Pollo', 45, 1, 45, 17, 51, 'PR-00051'),
(7, 'Milanesa de Pollo Fugazzetta', 65, 1, 65, 17, 53, 'PR-00053'),
(8, 'Huevo(uno)', 4, 1, 4, 18, 129, 'PR-00129'),
(9, 'Ensalada 3 Ingredientes', 45, 1, 45, 18, 102, 'PR-00102'),
(10, 'Acelga a la Crema', 38, 1, 38, 18, 91, 'PR-00091'),
(11, 'Arrollado de Pollo c/Rusa', 58, 3, 174, 18, 14, 'PR-00014'),
(12, 'Hamburquesa c/J Q y Lechuga', 40, 1, 40, 19, 133, 'PR-00133'),
(13, 'Ensalada Dietetica', 45, 1, 45, 19, 105, 'PR-00105'),
(14, '                Ñoquis de Papa', 60, 1, 60, 19, 58, 'PR-00058'),
(15, 'Acelga a la Crema', 38, 2, 76, 20, 91, 'PR-00091'),
(16, 'Bife de Chorizo 1/2', 68, 2, 136, 20, 73, 'PR-00073'),
(17, 'Hamburquesa c/J Q y Lechuga', 40, 2, 80, 21, 133, 'PR-00133'),
(18, 'Combo Spiedo s/Bebida', 150, 1, 150, 21, 16, 'PR-00016'),
(19, 'Combo Spiedo s/Bebida', 150, 1, 150, 22, 16, 'PR-00016'),
(20, '                Ñoquis de Papa', 60, 1, 60, 23, 58, 'PR-00058'),
(21, ' Budin de Pan p/Compartir', 35, 2, 70, 23, 117, 'PR-00117'),
(22, 'Hamburquesa c/J Q y Lechuga', 40, 1, 40, 24, 133, 'PR-00133'),
(23, 'Bife de Chorizo 1/2', 68, 1, 68, 24, 73, 'PR-00073'),
(24, 'Arrollado de Pollo c/Rusa', 58, 2, 116, 25, 14, 'PR-00014'),
(25, 'Hamburquesa c/Lechuga y Tomate', 30, 100, 3000, 26, 131, 'PR-00131'),
(26, 'Coca Cola 1.5 L.', 30, 1, 30, 27, 9, 'PR-00009'),
(27, 'Ensalada Dietetica', 45, 2, 90, 27, 105, 'PR-00105'),
(28, 'Agua Mineral', 25, 7, 175, 28, 8, 'PR-00008'),
(29, 'Hamburquesa c/Lechuga y Tomate', 30, 1, 30, 28, 131, 'PR-00131'),
(30, 'Hamburquesa c/Queso', 25, 1, 25, 28, 130, 'PR-00130'),
(31, 'Vino R. Famoso 3/4', 75, 6, 450, 29, 6, 'PR-00006'),
(32, 'Arrollado de Pollo p/2', 110, 1, 110, 29, 15, 'PR-00015'),
(33, 'Hamburquesa c/Queso', 25, 3, 75, 30, 130, 'PR-00130'),
(34, 'Bife de Chorizo 1/2', 68, 1, 68, 30, 73, 'PR-00073'),
(35, 'Ensalada Dietetica', 45, 1, 45, 31, 105, 'PR-00105'),
(36, '                Ñoquis de Papa', 60, 7, 420, 31, 58, 'PR-00058'),
(37, 'Milanesa de Pollo Napolitano', 65, 1, 65, 31, 52, 'PR-00052'),
(38, 'Coca Cola 2 L.', 35, 2, 70, 32, 7, 'PR-00007'),
(39, 'Hamburquesa c/Queso', 25, 4, 100, 32, 130, 'PR-00130'),
(40, 'Tiramisu', 35, 2, 70, 32, 108, 'PR-00108'),
(41, 'Hamburquesa c/J Q y Lechuga', 40, 2, 80, 33, 133, 'PR-00133'),
(42, 'Coca Cola 1.5 L.', 30, 1, 30, 33, 9, 'PR-00009'),
(43, 'Vasos(unidad)', 1, 5, 5, 33, 128, 'PR-00128'),
(44, 'Combo Spiedo s/Bebida', 150, 1, 150, 34, 16, 'PR-00016'),
(45, 'Hamburquesa c/J Q y Lechuga', 40, 2, 80, 34, 133, 'PR-00133'),
(46, 'Tiramisu', 35, 1, 35, 34, 108, 'PR-00108'),
(47, 'Ensalada 3 Ingredientes', 45, 1, 45, 34, 102, 'PR-00102');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE IF NOT EXISTS `producto` (
`producto_id` bigint(20) NOT NULL,
  `producto_codigo` varchar(20) DEFAULT NULL,
  `producto_nombre` varchar(255) DEFAULT NULL,
  `producto_descripcion` varchar(255) DEFAULT NULL,
  `producto_precio` int(11) DEFAULT NULL,
  `producto_fecha_ingreso` datetime DEFAULT NULL,
  `producto_fecha_modificacion` datetime DEFAULT NULL,
  `categoria_id` bigint(20) DEFAULT NULL,
  `unidad_venta_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`producto_id`, `producto_codigo`, `producto_nombre`, `producto_descripcion`, `producto_precio`, `producto_fecha_ingreso`, `producto_fecha_modificacion`, `categoria_id`, `unidad_venta_id`) VALUES
(6, 'PR-00006', 'Vino R. Famoso 3/4', '', 75, '2013-01-08 16:40:37', '2015-11-08 18:43:10', 2, 3),
(7, 'PR-00007', 'Coca Cola 2 L.', 'Coca Cola 2 L.', 35, '2015-09-26 16:36:09', '2015-11-08 19:21:50', 2, 3),
(8, 'PR-00008', 'Agua Mineral', 'Agua Mineral 1.5 L.', 25, '2015-11-08 17:41:05', '2015-11-08 17:41:31', 2, 3),
(9, 'PR-00009', 'Coca Cola 1.5 L.', 'Coca Cola 1.5 L.', 30, '2015-11-08 18:29:52', '2015-11-08 18:35:23', 2, 3),
(10, 'PR-00010', 'Vino Fino 3/4(de la Casa)', 'Vino Fino 3/4(de la Casa)', 40, '2015-11-08 18:37:02', '2015-11-08 18:37:58', 2, 3),
(11, 'PR-00011', 'Vino Lopez 3/4', '', 55, '2015-11-08 18:45:12', '2015-11-08 18:46:20', 2, 3),
(12, 'PR-00012', 'Cerveza Quilmes', '', 20, '2015-11-08 18:47:30', NULL, 2, 3),
(13, 'PR-00013', 'Gaseosa Speed 1/2 L.', '', 20, '2015-11-08 18:49:56', '2015-11-08 18:53:10', 2, 3),
(14, 'PR-00014', 'Arrollado de Pollo c/Rusa', '', 58, '2015-11-08 18:57:31', '2015-11-08 23:42:22', 3, 3),
(15, 'PR-00015', 'Arrollado de Pollo p/2', '', 110, '2015-11-08 18:59:26', NULL, 3, 3),
(16, 'PR-00016', 'Combo Spiedo s/Bebida', '', 150, '2015-11-08 19:05:03', '2015-11-08 23:26:36', 5, 3),
(48, 'PR-00048', 'Milanesa Ternera', '', 40, '2015-11-08 23:09:33', NULL, 8, 3),
(49, 'PR-00049', 'Milanesa Ternera Napolitana', '', 60, '2015-11-08 23:10:17', NULL, 8, 3),
(50, 'PR-00050', 'Milanesa Ternera Fugazzeta', '', 60, '2015-11-08 23:11:18', NULL, 8, 3),
(51, 'PR-00051', 'Milanesa de Pollo', '', 45, '2015-11-08 23:12:01', NULL, 8, 3),
(52, 'PR-00052', 'Milanesa de Pollo Napolitano', '', 65, '2015-11-08 23:12:57', NULL, 8, 3),
(53, 'PR-00053', 'Milanesa de Pollo Fugazzetta', '', 65, '2015-11-08 23:13:59', NULL, 8, 3),
(54, 'PR-00054', 'Milanesa de Pollo Suiza', '', 65, '2015-11-08 23:14:46', NULL, 8, 3),
(55, 'PR-00055', 'Milanesa de Soja', '', 35, '2015-11-08 23:15:33', NULL, 8, 3),
(56, 'PR-00056', 'Combo Ravioles p/3 personas', '', 210, '2015-11-09 00:03:17', NULL, 6, 3),
(57, 'PR-00057', 'Combo Ñoquis p/3 personas', '', 200, '2015-11-09 00:04:58', NULL, 6, 3),
(58, 'PR-00058', '                Ñoquis de Papa', '', 60, '2015-11-09 00:06:08', NULL, 6, 3),
(59, 'PR-00059', 'Fideos Salteados c/verdu', '', 65, '2015-11-09 00:07:55', NULL, 6, 3),
(61, 'PR-00061', 'Fideos Salteados c/Verdu p/Compar', '', 130, '2015-11-09 00:17:12', NULL, 6, 3),
(62, 'PR-00062', '  Ravioles Caseros', '', 65, '2015-11-09 00:19:17', NULL, 6, 3),
(63, 'PR-00063', 'Ravioles Caseros p/Compartir', '', 130, '2015-11-09 00:20:26', NULL, 6, 3),
(64, 'PR-00064', 'Tallarines al Huevo', '', 60, '2015-11-09 00:21:38', NULL, 6, 3),
(65, 'PR-00065', 'Tallarines al Huevo p/Compartir', '', 120, '2015-11-09 00:22:23', NULL, 6, 3),
(66, 'PR-00066', 'Lasagña Italiana c/Champiñones', '', 65, '2015-11-09 00:23:37', NULL, 6, 3),
(67, 'PR-00067', 'Lasagña Italiana p/Compartir', '', 120, '2015-11-09 00:24:33', NULL, 6, 3),
(68, 'PR-00068', 'Canelones de carne (porcion)', '', 65, '2015-11-09 00:25:26', NULL, 6, 3),
(69, 'PR-00069', 'Canelones p/Compartir', '', 120, '2015-11-09 00:26:24', NULL, 6, 3),
(70, 'PR-00070', 'Estofado 130gr', '', 30, '2015-11-09 00:27:38', NULL, 6, 3),
(71, 'PR-00071', 'Macarrones Graten de Verduras', '', 65, '2015-11-09 00:28:19', NULL, 6, 3),
(72, 'PR-00072', 'Matambre a la Pizza', '', 75, '2015-11-09 00:30:06', NULL, 7, 3),
(73, 'PR-00073', 'Bife de Chorizo 1/2', '', 68, '2015-11-09 00:31:07', NULL, 7, 3),
(74, 'PR-00074', 'Suprema Parrilla', '', 45, '2015-11-09 00:32:04', NULL, 7, 3),
(75, 'PR-00075', 'Chorizo', '', 30, '2015-11-09 00:33:03', NULL, 7, 3),
(76, 'PR-00076', 'Morcilla', '', 30, '2015-11-09 00:34:29', NULL, 7, 3),
(77, 'PR-00077', 'Pollo 1/2 a la Parrilla', '', 65, '2015-11-09 00:35:30', NULL, 7, 3),
(78, 'PR-00078', 'Pollo 1/2 a la Parrilla c/Guarnicion', '', 90, '2015-11-09 00:36:18', NULL, 7, 3),
(79, 'PR-00079', 'Brochette Mixta', '', 60, '2015-11-09 00:37:33', NULL, 7, 3),
(80, 'PR-00080', 'Chimichurri GDE', '', 15, '2015-11-09 00:38:29', NULL, 7, 3),
(81, 'PR-00081', 'Brochette c/Guarnicion', '', 80, '2015-11-09 00:39:19', NULL, 7, 3),
(82, 'PR-00082', 'Supremitas Dieteticas c/Guarnicion', '', 130, '2015-11-09 00:42:12', NULL, 7, 3),
(83, 'PR-00083', 'Combo Parrillero p4 con1/2 Pollo', '', 295, '2015-11-09 00:43:18', NULL, 7, 3),
(84, 'PR-00084', 'Choripan', '', 35, '2015-11-09 00:44:17', NULL, 7, 3),
(85, 'PR-00085', 'Tortilla de Papas', '', 45, '2015-11-09 01:03:48', NULL, 9, 3),
(86, 'PR-00086', 'Tortilla de Verduras', '', 45, '2015-11-09 01:04:45', NULL, 9, 3),
(87, 'PR-00087', 'Papas Dado', '', 30, '2015-11-09 01:05:27', NULL, 9, 3),
(88, 'PR-00088', 'Papas Fritas', '', 30, '2015-11-09 01:06:39', NULL, 9, 3),
(89, 'PR-00089', 'Arroz Primavera', '', 30, '2015-11-09 01:07:29', NULL, 9, 3),
(90, 'PR-00090', 'Verduras Salteadas', '', 30, '2015-11-09 01:08:16', NULL, 9, 3),
(91, 'PR-00091', 'Acelga a la Crema', '', 38, '2015-11-09 01:09:33', NULL, 9, 3),
(92, 'PR-00092', 'Pure de Papas', '', 30, '2015-11-09 01:10:17', NULL, 9, 3),
(93, 'PR-00093', 'Pure de Calabaza', '', 30, '2015-11-09 01:11:19', NULL, 9, 3),
(94, 'PR-00094', 'Pure Mixto', '', 30, '2015-11-09 01:12:05', NULL, 9, 3),
(95, 'PR-00095', 'Revuelto Gramajo', '', 38, '2015-11-09 01:12:58', NULL, 9, 3),
(96, 'PR-00096', 'Papas a la Crema', '', 30, '2015-11-09 01:13:45', NULL, 9, 3),
(97, 'PR-00097', 'Chop Suey', '', 30, '2015-11-09 01:14:31', NULL, 9, 3),
(98, 'PR-00098', 'Zanahorias a la Crema Porcion', '', 38, '2015-11-09 01:15:31', NULL, 9, 3),
(99, 'PR-00099', 'Bocadillos', '', 18, '2015-11-09 01:16:22', NULL, 9, 3),
(100, 'PR-00100', 'Papas y Batatas Horno', '', 30, '2015-11-09 01:17:14', NULL, 9, 3),
(101, 'PR-00101', 'Tortilla Española', '', 35, '2015-11-09 01:17:57', NULL, 9, 3),
(102, 'PR-00102', 'Ensalada 3 Ingredientes', '', 45, '2015-11-09 01:25:30', '2015-11-09 01:25:41', 4, 3),
(103, 'PR-00103', 'Ensalada Especial Salp/Rusa/Agus', '', 50, '2015-11-09 01:26:53', NULL, 4, 3),
(104, 'PR-00104', 'Ensalada p/Compartir LTZC', '', 45, '2015-11-09 01:28:13', NULL, 4, 3),
(105, 'PR-00105', 'Ensalada Dietetica', '', 45, '2015-11-09 01:29:06', NULL, 4, 3),
(106, 'PR-00106', 'Recargo Pollo', '', 20, '2015-11-09 01:30:00', NULL, 4, 3),
(107, 'PR-00107', ' Flan Casero', '', 25, '2015-11-09 01:32:07', NULL, 10, 3),
(108, 'PR-00108', 'Tiramisu', '', 35, '2015-11-09 01:32:54', NULL, 10, 3),
(109, 'PR-00109', 'Postre de Vainilla', '', 25, '2015-11-09 01:33:36', NULL, 10, 3),
(110, 'PR-00110', 'Tortas Varias', '', 35, '2015-11-09 01:35:03', NULL, 10, 3),
(111, 'PR-00111', 'Tarta de Manzana', '', 35, '2015-11-09 01:35:42', NULL, 10, 3),
(112, 'PR-00112', ' Crema Porcion', '', 20, '2015-11-09 01:36:21', NULL, 10, 3),
(113, 'PR-00113', 'Dulce de Leche', '', 20, '2015-11-09 01:37:13', NULL, 10, 3),
(114, 'PR-00114', 'Natillas', '', 25, '2015-11-09 01:37:49', NULL, 10, 3),
(115, 'PR-00115', 'Gelatina c/Frutas', '', 25, '2015-11-09 01:38:36', NULL, 10, 3),
(116, 'PR-00116', 'Manzana Asada', '', 25, '2015-11-09 01:39:15', NULL, 10, 3),
(117, 'PR-00117', ' Budin de Pan p/Compartir', '', 35, '2015-11-09 01:40:28', NULL, 10, 3),
(118, 'PR-00118', 'Tartaletas p/Compartir', '', 35, '2015-11-09 01:41:20', NULL, 10, 3),
(119, 'PR-00119', ' Ensalada de Frutas Promo 1/2Kg', '', 35, '2015-11-09 01:42:12', NULL, 10, 3),
(120, 'PR-00120', 'Queso y Dulce', '', 30, '2015-11-09 01:43:18', NULL, 10, 3),
(121, 'PR-00121', 'Pastelitos', '', 10, '2015-11-09 01:43:54', NULL, 10, 3),
(122, 'PR-00122', ' Arroz con Leche', '', 25, '2015-11-09 01:44:39', NULL, 10, 3),
(123, 'PR-00123', 'Pan', '', 3, '2015-11-09 01:47:00', NULL, 11, 3),
(124, 'PR-00124', 'Aderezos', '', 2, '2015-11-09 01:48:01', NULL, 11, 3),
(125, 'PR-00125', 'Queso Rallado(Sobre)', '', 15, '2015-11-09 01:48:58', NULL, 11, 3),
(126, 'PR-00126', 'Platos', '', 3, '2015-11-09 01:49:56', NULL, 11, 3),
(127, 'PR-00127', 'Cubiertos (par)', '', 2, '2015-11-09 01:50:47', NULL, 11, 3),
(128, 'PR-00128', 'Vasos(unidad)', '', 1, '2015-11-09 01:52:03', NULL, 11, 3),
(129, 'PR-00129', 'Huevo(uno)', '', 4, '2015-11-09 01:52:49', NULL, 11, 3),
(130, 'PR-00130', 'Hamburquesa c/Queso', '', 25, '2015-11-09 02:09:21', NULL, 1, 3),
(131, 'PR-00131', 'Hamburquesa c/Lechuga y Tomate', '', 30, '2015-11-09 02:10:11', '2015-11-09 02:24:13', 1, 3),
(132, 'PR-00132', 'Hamburquesa c/Jamon y Queso', '', 35, '2015-11-09 02:11:22', NULL, 1, 3),
(133, 'PR-00133', 'Hamburquesa c/J Q y Lechuga', '', 40, '2015-11-09 02:12:32', '2015-11-09 02:25:42', 1, 3),
(134, 'PR-00134', 'Sandwich Croissant c/Jamon y Queso', '', 35, '2015-11-09 02:14:04', NULL, 1, 3),
(135, 'PR-00135', 'Sandwich Saborizado Jamon y Queso', '', 35, '2015-11-09 02:15:09', NULL, 1, 3),
(136, 'PR-00136', 'Sandwich Triple Miga Jamon y Queso', '', 25, '2015-11-09 02:16:38', NULL, 1, 3),
(137, 'PR-00137', 'Sandwich Pavita Lechuga y Tomate', '', 38, '2015-11-09 02:19:26', NULL, 1, 3),
(138, 'PR-00138', 'Sandwich Milanesa Lechuga y Tomate', '', 38, '2015-11-09 02:20:37', NULL, 1, 3),
(139, 'PR-00139', 'Coca Colla Cero', '', 10, '2017-05-08 20:14:52', '2017-05-08 20:29:06', 2, 3),
(140, 'PR-00140', 'Coca Colla ', '', 10, '2017-05-08 20:17:46', NULL, 2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_oferta`
--

CREATE TABLE IF NOT EXISTS `producto_oferta` (
`producto_oferta_id` bigint(20) NOT NULL,
  `producto_precio_anterior` int(11) DEFAULT NULL,
  `producto_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto_oferta`
--

INSERT INTO `producto_oferta` (`producto_oferta_id`, `producto_precio_anterior`, `producto_id`) VALUES
(1, 80, 7),
(2, 20, 122),
(7, -22, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
`slider_id` bigint(20) NOT NULL,
  `slider_nombre_imagen` varchar(255) DEFAULT NULL,
  `slider_titulo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_nombre_imagen`, `slider_titulo`) VALUES
(17, 'S-000017-platos.png', 'Comidas'),
(18, 'S-000018-postres.png', 'Postres & Bocaditos'),
(19, 'S-000019-bebidas.png', 'Vinos & Bebidas'),
(20, 'S-000020-reunion.png', 'Eventos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE IF NOT EXISTS `tipo_documento` (
`tipo_documento_id` bigint(20) NOT NULL,
  `tipo_documento_nombre` varchar(255) DEFAULT NULL,
  `tipo_documento_descripcion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`tipo_documento_id`, `tipo_documento_nombre`, `tipo_documento_descripcion`) VALUES
(1, 'Boleta', 'Boleta'),
(2, 'Factura', 'Factura');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad_venta`
--

CREATE TABLE IF NOT EXISTS `unidad_venta` (
`unidad_venta_id` bigint(20) NOT NULL,
  `unidad_venta_nombre` varchar(255) DEFAULT NULL,
  `unidad_venta_descripcion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `unidad_venta`
--

INSERT INTO `unidad_venta` (`unidad_venta_id`, `unidad_venta_nombre`, `unidad_venta_descripcion`) VALUES
(1, 'Kilo', ''),
(2, 'Paquete', ''),
(3, 'Unidades', ''),
(4, 'Litros', 'Litros');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
 ADD PRIMARY KEY (`categoria_id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
 ADD PRIMARY KEY (`cliente_id`), ADD UNIQUE KEY `uq_cliente_email` (`cliente_email`);

--
-- Indices de la tabla `comuna`
--
ALTER TABLE `comuna`
 ADD PRIMARY KEY (`comuna_id`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
 ADD PRIMARY KEY (`configuracion_id`);

--
-- Indices de la tabla `direccion`
--
ALTER TABLE `direccion`
 ADD PRIMARY KEY (`direccion_id`), ADD KEY `fk_direccion_comuna_comuna_id` (`comuna_id`), ADD KEY `fk_direccion_cliente_cliente_id` (`cliente_id`);

--
-- Indices de la tabla `forma_pago`
--
ALTER TABLE `forma_pago`
 ADD PRIMARY KEY (`forma_pago_id`);

--
-- Indices de la tabla `imagen`
--
ALTER TABLE `imagen`
 ADD PRIMARY KEY (`imagen_id`), ADD KEY `fk_imagen_producto_producto_id` (`producto_id`);

--
-- Indices de la tabla `menu_panel_control_cliente`
--
ALTER TABLE `menu_panel_control_cliente`
 ADD PRIMARY KEY (`menu_panel_control_cliente_id`);

--
-- Indices de la tabla `pagina`
--
ALTER TABLE `pagina`
 ADD PRIMARY KEY (`pagina_id`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
 ADD PRIMARY KEY (`pedido_id`), ADD KEY `fk_cliente_pedido_cliente_id` (`cliente_id`), ADD KEY `fk_forma_pago_pedido_forma_pago_id` (`forma_pago_id`), ADD KEY `fk_pedido_tipo_documento_tipo_documento_id` (`tipo_documento_id`);

--
-- Indices de la tabla `pedido_detalle`
--
ALTER TABLE `pedido_detalle`
 ADD PRIMARY KEY (`pedido_detalle_id`), ADD KEY `fk_pedido_pedido_detalle_pedido__id` (`pedido_id`), ADD KEY `fk_pedido_detalle_producto_producto_id` (`producto_id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
 ADD PRIMARY KEY (`producto_id`), ADD UNIQUE KEY `uq_producto_codigo` (`producto_codigo`), ADD KEY `fk_producto_categoria_categoria_id` (`categoria_id`), ADD KEY `fk_producto_unidad_venta_unidad_venta_id` (`unidad_venta_id`);

--
-- Indices de la tabla `producto_oferta`
--
ALTER TABLE `producto_oferta`
 ADD PRIMARY KEY (`producto_oferta_id`), ADD UNIQUE KEY `uq_producto_id` (`producto_id`);

--
-- Indices de la tabla `slider`
--
ALTER TABLE `slider`
 ADD PRIMARY KEY (`slider_id`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
 ADD PRIMARY KEY (`tipo_documento_id`);

--
-- Indices de la tabla `unidad_venta`
--
ALTER TABLE `unidad_venta`
 ADD PRIMARY KEY (`unidad_venta_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
MODIFY `categoria_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
MODIFY `cliente_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25011;
--
-- AUTO_INCREMENT de la tabla `comuna`
--
ALTER TABLE `comuna`
MODIFY `comuna_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
MODIFY `configuracion_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `direccion`
--
ALTER TABLE `direccion`
MODIFY `direccion_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `forma_pago`
--
ALTER TABLE `forma_pago`
MODIFY `forma_pago_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `imagen`
--
ALTER TABLE `imagen`
MODIFY `imagen_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=203;
--
-- AUTO_INCREMENT de la tabla `menu_panel_control_cliente`
--
ALTER TABLE `menu_panel_control_cliente`
MODIFY `menu_panel_control_cliente_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `pagina`
--
ALTER TABLE `pagina`
MODIFY `pagina_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
MODIFY `pedido_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `pedido_detalle`
--
ALTER TABLE `pedido_detalle`
MODIFY `pedido_detalle_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
MODIFY `producto_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=141;
--
-- AUTO_INCREMENT de la tabla `producto_oferta`
--
ALTER TABLE `producto_oferta`
MODIFY `producto_oferta_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `slider`
--
ALTER TABLE `slider`
MODIFY `slider_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
MODIFY `tipo_documento_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `unidad_venta`
--
ALTER TABLE `unidad_venta`
MODIFY `unidad_venta_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--
--
-- Filtros para la tabla `imagen`
--
ALTER TABLE `imagen`
ADD CONSTRAINT `fk_imagen_producto_producto_id` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`producto_id`);


--
-- Filtros para la tabla `pedido_detalle`
--
ALTER TABLE `pedido_detalle`
ADD CONSTRAINT `fk_pedido_detalle_producto_producto_id` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`producto_id`),
ADD CONSTRAINT `fk_pedido_pedido_detalle_pedido__id` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`pedido_id`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
ADD CONSTRAINT `fk_producto_categoria_categoria_id` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`categoria_id`),
ADD CONSTRAINT `fk_producto_unidad_venta_unidad_venta_id` FOREIGN KEY (`unidad_venta_id`) REFERENCES `unidad_venta` (`unidad_venta_id`);

--
-- Filtros para la tabla `producto_oferta`
--
ALTER TABLE `producto_oferta`
ADD CONSTRAINT `fk_producto_oferta_producto_producto_id` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`producto_id`);

