<?php
/* @var $this ProductoOfertaController */
/* @var $model ProductoOferta */

$this->breadcrumbs=array(
	'Producto Ofertas'=>array('index'),
	$model->producto_oferta_id,
);

$this->menu=array(
	array('label'=>'Listar Productos de Oferta', 'url'=>array('index')),
	array('label'=>'Crear Productos de Oferta', 'url'=>array('create')),
	array('label'=>'Actualizar Productos de Oferta', 'url'=>array('update', 'id'=>$model->producto_oferta_id)),
	array('label'=>'Eliminar Productos de Oferta', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->producto_oferta_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Productos de Oferta', 'url'=>array('admin')),
);
?>

<h1>View ProductoOferta #<?php echo $model->producto_oferta_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'producto_oferta_id',
		'producto_precio_anterior',
		'producto_id',
	),
)); ?>
