<?php
/* @var $this ProductoOfertaController */
/* @var $model ProductoOferta */

$this->breadcrumbs=array(
	'Producto Ofertas'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Productos de Oferta', 'url'=>array('index')),
	array('label'=>'Crear Productos de Oferta', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('producto-oferta-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Productos de Ofertas</h1>

<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'producto-oferta-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'producto_oferta_id',
		'producto_precio_anterior',
		'producto_id',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
