<?php
/* @var $this ProductoOfertaController */
/* @var $model ProductoOferta */

$this->breadcrumbs=array(
	'Producto Ofertas'=>array('index'),
	$model->producto_oferta_id=>array('view','id'=>$model->producto_oferta_id),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Productos de Oferta', 'url'=>array('index')),
	array('label'=>'Crear Productos de Oferta', 'url'=>array('create')),
	array('label'=>'Ver Productos de Oferta', 'url'=>array('view', 'id'=>$model->producto_oferta_id)),
	array('label'=>'Administrar Productos de Oferta', 'url'=>array('admin')),
);
?>

<h1>Actualizar Productos de Oferta <?php echo $model->producto_oferta_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>