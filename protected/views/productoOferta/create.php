<?php
/* @var $this ProductoOfertaController */
/* @var $model ProductoOferta */

$this->breadcrumbs=array(
	'Producto Ofertas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Productos de Oferta', 'url'=>array('index')),
	array('label'=>'Administrar Productos de Oferta', 'url'=>array('admin')),
);
?>

<h1>Crear Productos de Oferta</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>