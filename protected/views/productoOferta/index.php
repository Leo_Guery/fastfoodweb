<?php
/* @var $this ProductoOfertaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Producto Ofertas',
);

$this->menu=array(
	array('label'=>'Crear Productos de Oferta', 'url'=>array('create')),
	array('label'=>'Administrar Productos de Oferta', 'url'=>array('admin')),
);
?>

<h1>Producto Ofertas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
