<?php
/* @var $this ProductoController */
/* @var $model Producto */

$this->breadcrumbs=array(
	'Productos'=>array('index'),
	$model->producto_id=>array('view','id'=>$model->producto_id),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Productos', 'url'=>array('index')),
	array('label'=>'Crear Productos', 'url'=>array('create')),
	array('label'=>'Ver Productos', 'url'=>array('view', 'id'=>$model->producto_id)),
	array('label'=>'Administrar Productos', 'url'=>array('admin')),
);
?>

<h1>Actualizar Producto <?php echo $model->producto_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>